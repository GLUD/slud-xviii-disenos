#!/bin/zsh

cd resultados

#leyendo numero de titulos
read num

for (( i=0;i<$num;i++ )); do
    read temas
    inkscape --export-png=$temas.png $temas.svg 2> /dev/null
    echo "[======================================================]"
done

#Borrando archivosw
rm ../temas
rm -r ./img
rm *.svg

echo ""
echo "      ====================="
echo "      | SVG exportados :) |"
echo "      ====================="

