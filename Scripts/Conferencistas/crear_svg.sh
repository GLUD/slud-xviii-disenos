#!/bin/zsh

#creando carpeta y copias
rm -rf resultados 2> /dev/null
mkdir resultados && cd resultados
cp -r ../img .

#Lectura de números
echo "Leyendo el número de conferencias a crear..."
read num
echo $num > ../titulos

for (( i=0;i<$num;i++ ))
do
    #Lectura de los datos
    read espacio
    echo "[==================================================]"

    echo "Leyendo el título de la conferencia..."
    read titulo

    echo "Leyendo el día de la conferencia..."
    read dia

    echo "Leyendo la hora de la conferencia..."
    read hora

    echo "Leyendo el nombre de la imagen..."
    read imagen

    #Procesos para el cambio de datos
    archivo=$titulo.svg
    cp ../base.svg ./$archivo

    sed -i 's/conferencia/'$titulo'/g' $archivo
    sed -i 's/dia/'$dia'/g' $archivo
    sed -i 's/hora/'$hora'/g' $archivo
    sed -i 's/img_base.png/'$imagen'/g' $archivo

    #añadir el titulo al archivo
    echo $titulo >> ../titulos
    
done

echo "------------------------------"
echo "| los svg han sido diseñados |"
echo "|    por favor revisarlos    |"
echo "------------------------------"
