## RECOMENDACIONES

 - Tener instalado el interprete de comandos ZSH instalado, dado que este interprete acepta espacios en las entradas.
 - En caso de no tenerlo instalado ejecutar `sudo apt install zsh` y ejecutar `zsh` en una terminal.
 - Evitar el uso de comillas dobles en el ingreso de datos. Los demás caracteres especiales son válidos.

## INSTRUCCIONES

1) Cambiar los espacios en el archivo `ingresar_datos`. Estos van sin comillas y tener cuidado con la sintaxis del tema.

2) Ejecutar el siguiente comando `cat ingresar_datos | ./crear_spam.sh`

3) Revisar en `.` el nuevo directorio creado llamado `resultados`
