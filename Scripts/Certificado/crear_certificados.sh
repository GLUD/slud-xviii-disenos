#!/bin/zsh

#haciendo lectura inicial
echo "Leyendo el número de días..."
read num_dias

echo "Leyendo día (Número) del primer día..."
read num_fecha
#num_fecha=23

#modificando carpeta
rm -rf resultados 2> /dev/null
mkdir resultados && cd resultados

for (( i=0;i<$num_dias;i++ ))
do
    #lectura de cada día
    read espacio
    echo "[======================================================]"
    echo "Leyendo número de personas para el día $(( $i+1 ))..."
    read num_personas

    for (( j=0;j<$num_personas;j++ ))
    do
        #lectura de nombre y céluda
        echo "Leyendo datos de la persona número $(( $j+1 ))..."
        read nombre
        read cedula
	read conferencia

        #copiando de svg
        archivo="${nombre} ${conferencia}.svg"
        cp ../certificado.svg $archivo

        #cambio en svg
        sed -i 's/nombre/'$nombre'/g' $archivo
        sed -i 's/cedula/'$cedula'/g' $archivo
	sed -i 's/conferencia/'$conferencia'/g' $archivo
        sed -i 's/num_dia/'$num_fecha'/g' $archivo

        #exportando svg
        inkscape --export-pdf="${nombre} - ${conferencia}.pdf" $archivo 2> /dev/null
    done

    num_fecha=$(( $num_fecha+1 ))
    
done

rm *.svg
