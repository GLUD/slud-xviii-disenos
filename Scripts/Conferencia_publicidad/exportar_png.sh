#!/bin/zsh

cd resultados

#leyendo numero de titulos
read num

for (( i=0;i<$num;i++ )); do

    read titulo
    inkscape --export-png=$titulo.png $titulo.svg 2> /dev/null
    echo "[======================================================]"
done

rm ../titulos
rm -r ./img
rm *.svg

echo ""
echo "      ====================="
echo "      | SVG exportados :) |"
echo "      ====================="
